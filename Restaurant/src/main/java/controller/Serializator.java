package controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.*;

public class Serializator {
	
	public final static void serialize(Restaurant restaurant) {
		
		try {
			
			FileOutputStream outputFile = new FileOutputStream("restaurant.ser");
			ObjectOutputStream serializationStream = new ObjectOutputStream(outputFile);
			serializationStream.writeObject(restaurant);
			serializationStream.close();
			outputFile.close();
			System.out.println("Serialization done!");
			
		}catch(IOException e ) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public final static Restaurant deserialize(String fileName) {

		Restaurant restaurant = new Restaurant();
		
		try {
			
			FileInputStream inputFile = new FileInputStream(fileName);
			ObjectInputStream deserializationStream = new ObjectInputStream(inputFile);
			restaurant = (Restaurant)deserializationStream.readObject();
			deserializationStream.close();
			inputFile.close();
			System.out.println("Deserialization done!");
		}catch(IOException e1) {
			System.out.println("IO Exception: " + e1.getMessage());
		}catch(ClassNotFoundException e2) {
			System.out.println("Class Not Found Exception: " + e2.getMessage());
		}
		
		return restaurant;
	}
}
