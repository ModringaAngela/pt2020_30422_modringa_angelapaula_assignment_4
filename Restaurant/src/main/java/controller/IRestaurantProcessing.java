package controller;

import model.*;

import java.io.IOException;
import java.lang.*;
import java.util.ArrayList;

public interface IRestaurantProcessing {

	/**
	 * @param item
	 * item != null
	 * list.size() before == list.size() after - 1
	 */
	public void addMenuItem(MenuItem item);
	
	
	/**
	 * @param item
	 * item != null
	 * list.size() before == list.size() after + 1
	 */
	public void deleteMenuItem(MenuItem item);
	
	
	/**
	 * @param item
	 * item != null
	 */
	public void editProductName(MenuItem item, String newName);
	
	
	/**
	 * @param item
	 * item != null
	 * newPrice != 0
	 */
	public void editProductPrice(MenuItem item, float newPrice);
	
	
	/**
	 * toEdit != null
	 * toDelete != null
	 * @param toEdit
	 * @param toDelete
	 */
	public void editCompositeProduct(CompositeProduct toEdit, MenuItem toDelete);
	
	
	/**
	 * @param order
	 * @param items
	 * order != null
	 * item != null
	 */
	public void createOrder(Order order, ArrayList<MenuItem> items);
	
	
	/**
	 * @param order
	 * order!= null
	 */
	public float computeOrderPrice(Order order);
	
	
	/**
	 * title != null
	 * message != null 
	 * @param title
	 * @param message
	 * @throws IOException
	 */
	public void generateBill(String title, String message) throws IOException;
}
