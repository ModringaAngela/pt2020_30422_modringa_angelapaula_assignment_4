package controller;

import java.awt.DisplayMode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.*;
import model.*;

public class Controller {
	
	private StartGUI start;
	
	private Restaurant restaurantField;
	
	private RestaurantGUI informations;
	
	public Controller(Restaurant restaurant) {
		
		restaurantField = restaurant;
		
		start = new StartGUI(restaurant);
		
		start.setVisible(true);
		
		addActionListeners();
		
	}
	
	public void addActionListeners() {
		
		this.start.administratorButtonListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdminController admin = new AdminController(restaurantField);
				start.setVisible(false);
			}
			
		});
		
		this.start.waiterButtonListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WaiterController waiter = new WaiterController(restaurantField);
				start.setVisible(false);
			}
		});
		
		this.start.saveActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Serializator.serialize(restaurantField);
				start.displayMessage("Operations performed are saved!");
			}
		});
		
		this.start.viewActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//start.setVisible(false);
				informations = new RestaurantGUI(restaurantField);
				informations.setVisible(true);
			}
			
		});
	}
	
}
