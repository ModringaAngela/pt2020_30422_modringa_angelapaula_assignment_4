package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Restaurant;
import view.AdministratorGUI;
import view.ItemGUI;
import view.ItemToDeleteGUI;

public class AdminController {
	
	private AdministratorGUI admin;
	private Restaurant restaurantField;
	
	
	public AdminController(Restaurant restaurant){
		
		restaurantField = restaurant;
		
		admin = new AdministratorGUI(restaurantField);
		admin.setVisible(true);

		this.admin.backListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.setVisible(false);
				Controller controller = new Controller(restaurantField);
			}
		});
		
		this.admin.createProductListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.setVisible(false);
				ItemController item = new ItemController(restaurantField);
			}
		});
		
		this.admin.deleteItemListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.setVisible(false);
				ItemToDeleteGUI delete = new ItemToDeleteGUI(restaurantField);
			}
		});
		
		this.admin.editItemListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.setVisible(false);
				ItemController item = new ItemController(restaurantField);
			}
		});
	}

}
