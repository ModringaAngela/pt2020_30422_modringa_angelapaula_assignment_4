package controller;

import view.ItemGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Restaurant;

public class ItemController {
	
	private ItemGUI item;
	private Restaurant restaurantField;
	
	public ItemController(Restaurant restaurant) {
		
		restaurantField = restaurant;
		
		item = new ItemGUI(restaurantField);
		item.setVisible(true);
		
		this.item.backListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				item.setVisible(false);
				AdminController controller = new AdminController(restaurantField);
				
			}
		});
	}
}
