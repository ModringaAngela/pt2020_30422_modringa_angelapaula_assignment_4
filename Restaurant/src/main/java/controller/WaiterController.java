package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import model.BadDateException;
import model.Date;
import model.MenuItem;
import model.Order;
import model.Restaurant;
import view.ItemGUI;
import view.WaiterGUI;

public class WaiterController {
	
	private WaiterGUI waiter;
	private Restaurant restaurantField;
	private ItemGUI item;
	
	public WaiterController(Restaurant restaurant) {
		
		restaurantField = restaurant;
		
		waiter = new WaiterGUI(restaurantField);
		item = new ItemGUI(restaurantField);
		waiter.setVisible(true);
		
		addListeners();
	}
	
	private void addListeners() {
		
		this.waiter.backListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				waiter.setVisible(false);
				Controller controller = new Controller(restaurantField);
			}
		});
		
		this.waiter.createOrderListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			try {
					int orderId = waiter.getNewOrderId();
					Integer tableNumber = waiter.getTableNumber();
					try {
						Date date = waiter.getOrderDate();
						Order newOrder = new Order(orderId, date, tableNumber);
						ArrayList<MenuItem> itemsOrdered = waiter.getItemsAdded();
						restaurantField.createOrder(newOrder, itemsOrdered);
						waiter.displayMessage("Order added!");
					}catch(BadDateException e) {
						waiter.displayMessage(e.getMessage());
					}
				}catch(NumberFormatException e) {
					waiter.displayMessage("Insert valid id");
				}
				
			}
		});
		
		this.waiter.generateBillListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int id = waiter.getOrderId();
				ArrayList<Order> allOrders = restaurantField.getOrders();
				Order order = null;
				for (Order o : allOrders) {
					if(o.getOrderId() == id) {
						order = o;
					}
				}
				float price = restaurantField.computeOrderPrice(order);
				String billText = "The bill for order " + id + " is " + price;
				try {
					restaurantField.generateBill("Bill for order " + id, billText);
				} catch (IOException e) {
					waiter.displayMessage(e.getMessage());
				}
			}
		});
		
		this.waiter.addListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				waiter.setItemsAdded();
			}
		});
		
		this.waiter.computePriceListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
	}
}
