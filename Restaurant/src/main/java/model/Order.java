package model;

import java.io.Serializable;

public class Order implements Serializable{
	
	private int orderId;
	private Date orderDate;
	private int tableNumber;
	
	public Order(int orderId, Date orderDate, int tableNumber) {
		super();
		this.orderId = orderId;
		this.orderDate = orderDate;
		this.tableNumber = tableNumber;
	}

	public int getOrderId() {
		return orderId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public int getTableNumber() {
		return tableNumber;
	}
	
	
	@Override
	public int hashCode() {
		int value = 3;
		int c1 = 5, c2 = 7, c3 = 11, c4 = 13;
		value += value * c1 + orderId * c2 + orderDate.getDay() * c3 + tableNumber *c4;
		return value;
	}

	
	@Override
	public boolean equals(Object object){
		
		if(this == object)
			return true;
		
		if(object == null) 
			return false;
		
		Order order = (Order) object;
		
		if(object.getClass() != this.getClass())
			return false;
		
		if(order.getOrderDate() != this.getOrderDate() || order.getOrderId() != this.getOrderId() || order.getTableNumber() != this.getTableNumber())
			return false;
		
		return true;
		
	}
}
