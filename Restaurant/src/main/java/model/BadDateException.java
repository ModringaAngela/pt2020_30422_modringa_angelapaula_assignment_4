package model;

public class BadDateException extends Exception{
	
	public BadDateException(String message){
		super(message);
	}

}
