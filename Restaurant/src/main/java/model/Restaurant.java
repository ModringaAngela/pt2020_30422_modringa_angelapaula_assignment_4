package model;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import controller.IRestaurantProcessing;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable{

	private static final long serialVersionUID = 4585823575666044265L;
	private ArrayList<MenuItem> theMenu = new ArrayList<MenuItem>();
	private ArrayList<Order> orders = new ArrayList<Order>();
	private HashMap<Order, ArrayList<MenuItem>> orderItemsMap = new HashMap<Order, ArrayList<MenuItem>>();
	
	
	public Restaurant() {
	}
	
	public ArrayList<MenuItem> getTheMenu() {
		return theMenu;
	}

	public ArrayList<Order> getOrders() {
		return orders;
	}

	public HashMap<Order, ArrayList<MenuItem>> getRestaurantInformations() {
		return orderItemsMap;
	}

	
	public void addMenuItem(MenuItem newItem) {
		
		assert newItem != null;
		
		int sizeBeforeAdd = theMenu.size();
		
		theMenu.add(newItem);
		
		int sizeAfterAdd = theMenu.size();
		
		assert sizeBeforeAdd == sizeAfterAdd - 1;
		
	}
	
	public void deleteMenuItem(MenuItem item) {
		
		assert item != null;
		
		int sizeBeforeDelete = theMenu.size();
		theMenu.remove(item);
		int sizeAfterDelete = theMenu.size();
		
		assert sizeBeforeDelete == sizeAfterDelete + 1;
		
	}
	
	public void editProductPrice(MenuItem item, float newPrice) {
		
		assert item != null;
		
		assert newPrice > 0;
	
		item.setPrice(newPrice);
		
	}

	public void editProductName(MenuItem item, String newName){
		
		assert item != null;
	
		item.setName(newName);
}
	
	public void editCompositeProduct(CompositeProduct toEdit, MenuItem toDelete) {
		
		assert toEdit != null : "the element you want to edit can't be null";
		assert toDelete != null : "the element you want to delete can't be null";
		
		toEdit.deleteComponent(toDelete);
	}

	public void createOrder(Order order, ArrayList<MenuItem> items) {
		
		assert order != null : "order can t be null";
		assert items != null : "items from order can t be null";
		
		orders.add(order);
		
		try {
			System.out.println("Adding a new order to restaurant");
			orderItemsMap.put(order, items);
			System.out.println("Order added");
		}catch(Exception e) {
			System.out.println("Exception occured");
			System.out.println(e.getCause());
		}
		
		setChanged();
		
		notifyObservers();
		 
	}

	public float computeOrderPrice(Order order) {
		
		assert order != null;
		
		float price = 0;
		if (this.orders.contains(order)) {
			for(MenuItem item : orderItemsMap.get(order)) {
				price += item.getPrice();
			}
		}
		
		return price;
	}

	public void generateBill(String title, String message) throws IOException {
		
		assert title != null;
		assert message != null;
		
		FileWriter outputFile = new FileWriter(title);
		outputFile.write(message);
		outputFile.flush();
		outputFile.close();
		
	}
	
	
}
