package model;

import java.io.Serializable;

public class Date implements Serializable {
	
	private int day;
	private int month;
	private int year;
	
	public Date(int day, int month, int year) throws BadDateException {
		if(year < 2020 || year > 2021) {
			throw new BadDateException("The system is configured only for the years 2020 and 2021!");
		}
		if(month < 1 || month > 12) {
			throw new BadDateException("The month should be between 1 and 12!");
		}
		if(day < 1 || day > 31) {
			throw new BadDateException("The day should be between 1 and 31!");
		}
		if(((month == 4 || month == 6 || month == 9 || month == 11) && day > 30 )|| (month == 2 && day > 29)){
			throw new BadDateException("The date is not a valid one!");
		}
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}
	
	
	
}
