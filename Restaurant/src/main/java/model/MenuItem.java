package model;
import java.io.Serializable;

public abstract class MenuItem implements Serializable {
	
	private String name;
	private float price;
	
	public MenuItem() {
		
	}
	
	public MenuItem(String name, float price) {
		super();
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public abstract float computePrice();
	
	@Override
	public String toString() {
		
		return this.name;
	}
	
}
