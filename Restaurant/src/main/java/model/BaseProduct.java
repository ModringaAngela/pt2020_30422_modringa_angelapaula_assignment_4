package model;
import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable {

	public BaseProduct(String name, float price) {
		super(name, price);
	}

	@Override
	public float computePrice() {
		return getPrice();
	}
}
