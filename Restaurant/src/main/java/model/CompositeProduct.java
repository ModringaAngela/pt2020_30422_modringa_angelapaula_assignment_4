package model;
import java.util.ArrayList;
import model.MenuItem;
import java.io.Serializable;

public class CompositeProduct extends MenuItem implements Serializable {
	
	private ArrayList<MenuItem> components;
	
	public CompositeProduct() {
		
	}
	
	public CompositeProduct(String name, ArrayList<MenuItem> components) {
		super();
		this.components = new ArrayList<MenuItem>();
		this.setName(name);
		this.components = components;
	}
	
	@Override
	public float computePrice() {
		float price = 0 ;
		for(MenuItem product : components) {
			price += product.computePrice();
		}
		return price;
	}

	public void setPrice() {
		super.setPrice(this.computePrice());
	}
	
	public void addComponent(MenuItem component) {
		this.components.add(component);
	}
	
	public ArrayList<MenuItem> getComponents(){
		return this.components;
	}
	
	public void deleteComponent(MenuItem component) {
		this.components.remove(component);
	}
}
