package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import controller.AdminController;
import model.MenuItem;
import model.Restaurant;

public class ItemToDeleteGUI extends GUI{
	
	private JComboBox<MenuItem> items = new JComboBox<MenuItem>();
	private JButton delete = new JButton("Delete");
	private JButton back = new JButton("Back");
	
	private Restaurant restaurantField;
	
	public ItemToDeleteGUI(Restaurant restaurant){
		
		super(300, 200, "DELETE ITEM");
		
		this.restaurantField = restaurant;
		
		JPanel delete = new JPanel();
		delete.setLayout(null);
		
		this.setVisible(true);
		
		this.setBoundsForElements();
		this.addToPanel(delete);
		
		update();
		
		this.delete.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				restaurantField.deleteMenuItem((MenuItem)items.getSelectedItem());
				displayMessage("Item deleted");
				update();
			}
			
		});
		
		this.back.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AdminController controller = new AdminController(restaurantField);
			}
		});
		
		this.add(delete);
	}
	
	public void update() {
		
		ArrayList<MenuItem> itemsArray = restaurantField.getTheMenu();
		
		for(MenuItem m : itemsArray) {
			items.addItem(m);
		}
	}

	@Override
	public void setBoundsForElements() {
		items.setBounds(20, 20, 150, 30);
		delete.setBounds(20, 100, 70, 30);
		back.setBounds(200, 20, 70, 30);
		
	}

	@Override
	public void addToPanel(JPanel panel) {
		panel.add(items);
		panel.add(delete);
		panel.add(back);
		
	}

}
