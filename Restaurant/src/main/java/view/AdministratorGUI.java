package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Restaurant;

public class AdministratorGUI extends GUI {
	
	private Restaurant restaurant;
	
	private JButton createProduct = new JButton("Create Item");
	private JButton editItem= new JButton("Edit Item");
	private JButton deleteItem = new JButton("Delete Item");
	private JButton back = new JButton("Back");
	
	public AdministratorGUI(Restaurant restaurant) {
		
		super(410, 250, "ADMINISTRATOR");
		
		this.restaurant = restaurant;
		
		JPanel adminPanel = new JPanel();
		adminPanel.setLayout(null);
		
		this.setBoundsForElements();
		this.addToPanel(adminPanel);
		
		this.add(adminPanel);
	}
	
	@Override
	public void setBoundsForElements() {
		back.setBounds(300, 20, 70, 30);
		createProduct.setBounds(20, 50, 150, 30);
		editItem.setBounds(20, 100, 150, 30);
		deleteItem.setBounds(20, 150, 150, 30);
	}
	
	@Override
	public void addToPanel(JPanel adminPanel) {
		
		adminPanel.add(createProduct);
		adminPanel.add(editItem);
		adminPanel.add(deleteItem);
		adminPanel.add(back);
		
	}
	
	public void createProductListener(ActionListener listener) {
		createProduct.addActionListener(listener);
	}
	
	public void editItemListener(ActionListener listener) {
		editItem.addActionListener(listener);
	}
	
	public void deleteItemListener(ActionListener listener) {
		deleteItem.addActionListener(listener);
	}
	
	public void backListener(ActionListener listener) {
		back.addActionListener(listener);
	}

}
