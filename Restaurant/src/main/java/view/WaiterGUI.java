package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;

import controller.Serializator;
import model.BadDateException;
import model.Date;
import model.MenuItem;
import model.Order;
import model.Restaurant;

public class WaiterGUI extends GUI {
	
	private Restaurant restaurantField;
	
	private ArrayList<MenuItem> itemsSelected = new ArrayList<MenuItem>();
	
	private JButton createOrder = new JButton("Create Order");
	private JButton computePrice = new JButton("Total Price");
	private JButton generateBill= new JButton("Create Bill");
	private JButton back = new JButton("Back");
	private JButton add = new JButton("Add");
	
	private JTextField newOrderId = new JTextField();
	private JTextField orderDate = new JTextField();
	private JTextField orderId = new JTextField();
	private JTextField totalPrice = new JTextField();
	
	private JTextArea itemsAdded = new JTextArea();
	
	private JComboBox menuItem;
	private JComboBox tableNumber;
	
	private JLabel newIdLabel = new JLabel("Id:");
	private JLabel idLabel = new JLabel("Id:");
	private JLabel dateLabel = new JLabel("Date:");
	private JLabel tableLabel = new JLabel("Table:");
	private JLabel itemLabel = new JLabel("Item:");
	private JLabel totalPriceLabel = new JLabel("Total:");
	
	private int nrTables = 15;
	
	public WaiterGUI(Restaurant restaurant) {
		
		super(570, 500, "WAITER");
		
		this.restaurantField = restaurant;
		
		JPanel waiterPanel = new JPanel();
		waiterPanel.setLayout(null);
		
		Integer tables[] = new Integer[nrTables];
		for(int i = 1 ; i <= nrTables ; i++) {
			tables[i-1] = i;
		}
		
		tableNumber = new JComboBox(tables);
		menuItem = new JComboBox();
		
		ArrayList<MenuItem> menu = restaurantField.getTheMenu();
		int nrItems = menu.size();
		
		for(int i = 0 ; i < nrItems ; i++) {
			menuItem.addItem(menu.get(i));
		}

		this.setBoundsForElements();
		this.addToPanel(waiterPanel);
		
		addListenersForButtons();
		
		this.add(waiterPanel);
	}
	
	@Override
	public void setBoundsForElements() {
		//comboBox
		tableNumber.setBounds(70 , 100, 50, 30);
		menuItem.setBounds(70, 250, 100, 30);
		
		//buttons
		createOrder.setBounds(20, 350, 150, 30);
		computePrice.setBounds(380, 300, 150, 30);
		generateBill.setBounds(380, 400, 150, 30);
		back.setBounds(460, 20, 70, 30);
		add.setBounds(20, 300, 70, 30);
		
		//labels
		newIdLabel.setBounds(20, 50, 50, 30);
		tableLabel.setBounds(20 , 100, 50, 30);
		dateLabel.setBounds(20, 150, 150, 30);
		idLabel.setBounds(380, 250, 50, 30);
		itemLabel.setBounds(20, 250, 70, 30);
		totalPriceLabel.setBounds(380, 350, 70, 30);
		
		//text fields
		orderId.setBounds(430, 250 ,50, 30);
		newOrderId.setBounds(70, 50, 50, 30);
		orderDate.setBounds(70, 150, 150, 30);
		totalPrice.setBounds(450, 350, 50, 30);
		
		//text area
		itemsAdded.setBounds(180, 250, 100, 100);
	}

	@Override
	public void addToPanel(JPanel waiterPanel) {
		
		waiterPanel.add(idLabel);
		waiterPanel.add(newIdLabel);
		waiterPanel.add(dateLabel);
		waiterPanel.add(tableLabel);
		waiterPanel.add(itemLabel);
		waiterPanel.add(totalPriceLabel);
		
		waiterPanel.add(orderId);
		waiterPanel.add(orderDate);
		waiterPanel.add(newOrderId);
		waiterPanel.add(totalPrice);
		
		waiterPanel.add(itemsAdded);
		
		waiterPanel.add(createOrder);
		waiterPanel.add(computePrice);
		waiterPanel.add(generateBill);
		waiterPanel.add(add);
		waiterPanel.add(back);
		
		waiterPanel.add(menuItem);
		waiterPanel.add(tableNumber);
		
		
	}
	
	public void addListenersForButtons() {
		
		this.computePrice.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String orderIdString = orderId.getText();
				if(!orderIdString.isEmpty()) {
					try {
						Integer id = Integer.valueOf(orderIdString);
						ArrayList<Order> orders = restaurantField.getOrders();
						
						for(Order o : orders) {
							if(o.getOrderId() == id) {
								
								float price = restaurantField.computeOrderPrice(o);
								totalPrice.setText(String.valueOf(price));
								
							}
						}
					}catch(NumberFormatException ex) {
						displayMessage("Id is not an integer number!");
					}
				}
				else {
					displayMessage("Fill in the order id!");
				}
			}
			
		});
		
		this.generateBill.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				try {
					String title = "Bill for order " + orderId.getText();
					String text = "The total is :" + totalPrice.getText();
					restaurantField.generateBill(title, text);
					displayMessage("Bill created!");
				}catch(IOException e) {
					displayMessage(e.getMessage());
				}
				
			}
			
		});
	}
	
	////GETTERS 
	
	public Integer getOrderId() {
		return Integer.valueOf(this.orderId.getText());
	}
	
	public Integer getNewOrderId() {
		return Integer.valueOf(this.newOrderId.getText());
	}
	
	public Date getOrderDate() throws BadDateException {
		Date date = null;
		String dateString = this.orderDate.getText();
		String[] dateComponents = dateString.split("\\.");
		date = new Date(Integer.valueOf(dateComponents[0]), Integer.valueOf(dateComponents[1]), Integer.valueOf(dateComponents[2]));
		return date;
	}
	
	public Integer getTableNumber() {
		Integer tableNo = (Integer) this.tableNumber.getSelectedItem();
		return tableNo;
	}
	
	public ArrayList<MenuItem> getItemsAdded(){
		return this.itemsSelected;
	}
	
	/////SETTER
	
	public void setItemsAdded() {
		itemsSelected.add((MenuItem)this.menuItem.getSelectedItem());
		this.itemsAdded.append(((MenuItem)this.menuItem.getSelectedItem()).getName() + "\n");
	}
	
	///////LISTENERS 
	
	public void createOrderListener(ActionListener listener) {
		createOrder.addActionListener(listener);
	}
	
	public void computePriceListener(ActionListener listener) {
		computePrice.addActionListener(listener);
	}
	
	public void generateBillListener(ActionListener listener) {
		generateBill.addActionListener(listener);
	}
	
	public void backListener(ActionListener listener) {
		back.addActionListener(listener);
	}	
	
	public void addListener(ActionListener listener) {
		add.addActionListener(listener);
	}
}
