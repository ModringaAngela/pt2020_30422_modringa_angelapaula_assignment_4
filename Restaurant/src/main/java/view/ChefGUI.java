package view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Restaurant;

public class ChefGUI extends JFrame implements Observer{
	
	private JLabel text = new JLabel("CHEF SHOULD COOK!");
	
	public ChefGUI() {
		
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setSize(300, 300);
		this.setTitle("CHEF");
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		text.setBounds(40, 40, 200,30);
		panel.add(text);
		this.add(panel);
	}

	public void update(Observable o, Object arg) {
		
		setVisible(true);

	}

}
