package view;

import java.util.Map.Entry;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.Controller;
import model.Order;
import model.MenuItem;
import model.Restaurant;

public class RestaurantGUI extends GUI{
	
	private JTable ordersWithData = new JTable();
	
	private Restaurant restaurantField;
	
	public RestaurantGUI(Restaurant restaurant) {
		
		super(500, 500, "ORDERS");
		
		this.restaurantField = restaurant;
		
		JPanel tabel = new JPanel();
		tabel.setLayout(new BorderLayout());
		
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		ArrayList<Order> orders = restaurant.getOrders();
		
		HashMap<Order, ArrayList<MenuItem>> informations = restaurant.getRestaurantInformations();
		
		String[] infoOnRow = new String[4];
		int ct = 0;
		int size = informations.keySet().size();
		String[] columnNames = {"Order id", "Order date", "Table", "Items Ordered"};
		javax.swing.table.DefaultTableModel model = new javax.swing.table.DefaultTableModel(columnNames, 0);
		
		for(Entry<Order, ArrayList<MenuItem>> entry: informations.entrySet()) {
			
				Order order = entry.getKey();
				String day = String.valueOf(order.getOrderDate().getDay());
				String month = String.valueOf(order.getOrderDate().getMonth());
				String year = String.valueOf(order.getOrderDate().getYear());
				String dateString = day + "." + month + "." + year;
				String idString = String.valueOf(order.getOrderId());
				String tableString = String.valueOf(order.getTableNumber());
		       
		        ArrayList<MenuItem> items = entry.getValue();
		        String itemsString = "";
		        
		        for(int i = 0 ; i < items.size() ; i++ ) {
		        	 itemsString += items.get(i).getName() + " ";
		        }
		        
		        ct++;
		        infoOnRow[0] = idString;
		        infoOnRow[1] = dateString;
		        infoOnRow[2] = tableString;
		        infoOnRow[3] = itemsString;
		        
		        model.addRow(infoOnRow);
		}
		
		ordersWithData = new JTable(model);
		
		setBoundsForElements();
		
		addToPanel(tabel);
		
		this.add(tabel);
		
	}

	@Override
	public void setBoundsForElements() {
		
		ordersWithData.setBounds(20, 20, 300, 400);
	}

	@Override
	public void addToPanel(JPanel panel) {
		
		panel.add(ordersWithData);
		panel.add(ordersWithData.getTableHeader(), BorderLayout.NORTH);
	}
}
