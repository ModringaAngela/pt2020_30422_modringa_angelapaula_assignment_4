package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Serializator;
import model.BaseProduct;
import model.CompositeProduct;
import model.InvalidOperationException;
import model.MenuItem;
import model.Restaurant;

public class ItemGUI extends GUI{
	
	private ArrayList<MenuItem> componentsArray = new ArrayList<MenuItem>();
	
	private Restaurant restaurantField;
	
	private JTextField baseNameTF = new JTextField();
	private JTextField compositeNameTF = new JTextField();
	private JTextField priceTF = new JTextField();
	
	private JTextField newNameBaseTF = new JTextField();
	private JTextField newNameCompositeTF = new JTextField();
	private JTextField newPriceTF = new JTextField();
	
	private JLabel baseNameLabel = new JLabel("Name:");
	private JLabel compositeNameLabel = new JLabel("Name:");
	private JLabel priceLabel = new JLabel("Price:");
	private JLabel products = new JLabel("Products:");
	private JLabel createBaseLabel = new JLabel("CREATE BASE PRODUCT: ");
	private JLabel createCompositeLabel = new JLabel("CREATE COMPOSITE PRODUCT: ");
	private JLabel editBaseProductLabel = new JLabel("EDIT BASE PRODUCT: ");
	private JLabel editCompositeProductLabel = new JLabel("EDIT COMPOSITE PRODUCT: ");
	
	private JLabel baseProductToEditLabel = new JLabel("Product:");
	private JLabel newNameBaseLabel = new JLabel("New Name:");
	private JLabel newPriceBaseLabel = new JLabel("New Price:");
	
	private JLabel newNameCompositeLabel = new JLabel("New Name:");
	private JLabel compositeProductToEditLabel = new JLabel("Product:");
	private JLabel componentsLabel = new JLabel("Components:");
	
	private JButton editBase = new JButton("Edit");
	private JButton editComposite = new JButton("Edit");
	private JButton deleteSelected = new JButton("Delete");
	/////
	
	private JButton createBase = new JButton("Create");
	private JButton createComposite = new JButton("Create");
	private JButton back = new JButton("Back");
	private JButton add = new JButton("Add");
	
	private JComboBox menuItems;
	private JComboBox baseProductsToEdit;
	private JComboBox compositeProductsToEdit;
	private JComboBox baseProdInCompositeProdSelected;
	
	
	public ItemGUI(Restaurant restaurant){
		
		super(570, 600, "ITEM");
		
		this.restaurantField = restaurant;
		
		JPanel itemPanel = new JPanel();
		itemPanel.setLayout(null);
		
		menuItems = new JComboBox<MenuItem>();
		baseProductsToEdit = new JComboBox<BaseProduct>();
		compositeProductsToEdit = new JComboBox<CompositeProduct>();
		baseProdInCompositeProdSelected = new JComboBox<BaseProduct>();

		updateProducts();
		
		this.setBoundsForElements();
		
		this.addToPanel(itemPanel);
		
		addActionListeners();

		this.add(itemPanel);
	}
	
	public void setBoundsForTextFields() {
	
		baseNameTF.setBounds(70, 70, 100, 30);
		priceTF.setBounds(70, 120, 70, 30);
		compositeNameTF.setBounds(300, 70, 100, 30);
		products.setBounds(250, 120, 70, 30);
		newNameBaseTF.setBounds(100,  320, 100, 30);
		newPriceTF.setBounds(100, 370 , 70 ,30 );
		newNameCompositeTF.setBounds(330, 320, 100, 30);
		
	}
	
	public void setBoundsForLabels() {
		
		baseNameLabel.setBounds(20, 70, 50, 30);
		priceLabel.setBounds(20, 120, 50, 30);
		baseProductToEditLabel.setBounds(20, 270 , 70 ,30);
		newNameBaseLabel.setBounds(20, 320, 70, 30);
		newPriceBaseLabel.setBounds(20, 370 , 70 ,30);
		compositeProductToEditLabel.setBounds(250, 270 , 70 ,30);
		newNameCompositeLabel.setBounds(250, 320, 70, 30);
		componentsLabel.setBounds(250, 370 , 100 ,30);
		createBaseLabel.setBounds(20, 20, 200, 30);
		createCompositeLabel.setBounds(250, 20, 200, 30);
		editBaseProductLabel.setBounds(20, 230, 200, 30);
		editCompositeProductLabel.setBounds(250, 230, 200, 30);
		compositeNameLabel.setBounds(250, 70, 50, 30);
	}
	
	public void setBoundsForButtons() {
		
		add.setBounds(440, 120, 70, 30);
		back.setBounds(460, 500, 70, 30);
		createBase.setBounds(20, 170, 100, 30);
		editBase.setBounds(20, 420, 70, 30);
		editComposite.setBounds(250, 420, 70, 30);
		deleteSelected.setBounds(470 , 370 , 70, 30);
		createComposite.setBounds(250, 170, 100, 30);
	}
	
	public void setBoundsForComboBoxes() {
		
		baseProductsToEdit.setBounds(100, 270, 100, 30);
		compositeProductsToEdit.setBounds(330, 270, 100, 30);
		baseProdInCompositeProdSelected.setBounds(330, 370, 100, 30);
		menuItems.setBounds(320, 120, 100, 30);
	}
	
	
	public void setBoundsForElements() {
		
		setBoundsForButtons();
		setBoundsForComboBoxes();
		setBoundsForLabels();
		setBoundsForTextFields();
	}
	
	@Override
	public void addToPanel(JPanel itemPanel) {
		
		itemPanel.add(baseNameLabel);
		itemPanel.add(compositeNameLabel);
		itemPanel.add(priceLabel);
		
		itemPanel.add(baseProductToEditLabel);
		itemPanel.add(compositeProductToEditLabel);
		itemPanel.add(newNameCompositeLabel);
		itemPanel.add(newNameBaseLabel);
		itemPanel.add(newPriceBaseLabel);
		itemPanel.add(componentsLabel);
		
		itemPanel.add(editBase);
		itemPanel.add(editComposite);
		itemPanel.add(deleteSelected);
		
		itemPanel.add(createBaseLabel);
		itemPanel.add(createCompositeLabel);
		itemPanel.add(editBaseProductLabel);
		itemPanel.add(editCompositeProductLabel);
		
		itemPanel.add(priceTF);
		itemPanel.add(baseNameTF);
		itemPanel.add(compositeNameTF);
		itemPanel.add(products);
		itemPanel.add(menuItems);
		
		itemPanel.add(createBase);
		itemPanel.add(createComposite);
		itemPanel.add(back);
		itemPanel.add(add);
		
		itemPanel.add(baseProdInCompositeProdSelected);
		itemPanel.add(compositeProductsToEdit);
		itemPanel.add(baseProductsToEdit);
		
		itemPanel.add(newNameCompositeTF);
		itemPanel.add(newNameBaseTF);
		itemPanel.add(newPriceTF);
		
	}
	
	public void updateProducts() {
		
		ArrayList<MenuItem> menu = restaurantField.getTheMenu();
		ArrayList<CompositeProduct> compositeProductsArray = new ArrayList<CompositeProduct>();
		
	    int nrItems = menu.size();
	    
		for(int i = 0 ; i < nrItems ; i++) {
			Class itemClass = menu.get(i).getClass();
			if(itemClass.getName().equals("model.CompositeProduct")){
				compositeProductsArray.add((CompositeProduct)menu.get(i));
			}
		}
		
		for(int i = 0 ; i < nrItems ; i++) {
			
			MenuItem item = menu.get(i);
			String toAdd = item.getName();
			
			boolean exists = false;
			for(int j = 0 ; i < menuItems.getItemCount(); i++) {
				String existing = ((BaseProduct)menuItems.getItemAt(j)).getName();
				if(existing.equals(toAdd)) {
					exists = true;
				}
			}
			if(!exists) {
				menuItems.addItem(item);
				baseProductsToEdit.addItem(menu.get(i));
			}
		}
		
		for(int i = 0 ; i < compositeProductsArray.size() ; i++) {
			String toAdd = compositeProductsArray.get(i).getName();
			boolean exists = false;
			for(int j = 0 ; j < compositeProductsToEdit.getItemCount() ; j++) {
				String existing = ((CompositeProduct)compositeProductsToEdit.getItemAt(j)).getName();
				
				if(existing.equals(toAdd)) {
					exists = true;
				}
			}
			if(!exists) {
				compositeProductsToEdit.addItem(compositeProductsArray.get(i));
			}
		}
	}
	
	public void addActionListeners() {
		add.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				componentsArray.add((MenuItem)menuItems.getSelectedItem());
				displayMessage(menuItems.getSelectedItem().toString() + " added in " + compositeNameTF.getText());
			}
			
		});
		
		createComposite.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				String name = compositeNameTF.getText();
				if(!name.isEmpty()) {
					
					CompositeProduct newItem = new CompositeProduct(name, componentsArray);
					newItem.setPrice();
					restaurantField.addMenuItem(newItem);
					
					displayMessage("Composite product " + name + " added");
					compositeNameTF.setText(null);
					
					updateProducts();
					
				}else {
					displayMessage("Name of the product can't be empty");
				}
			}
			
		});
		
		createBase.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String name = baseNameTF.getText();
				if(!name.isEmpty()) {
					String priceString = priceTF.getText();
					if(!priceString.isEmpty()) {
						try {
							float price = Float.parseFloat(priceString);
							BaseProduct newItem = new BaseProduct(name, price);
							
							restaurantField.addMenuItem(newItem);
							
							displayMessage("Base Product " + name + " added");
							baseNameTF.setText(null);
							priceTF.setText(null);
							
							updateProducts();
							
						}catch(NumberFormatException e) {
							displayMessage("Insert a valid price!");
						}
					}
					else {
						displayMessage("Price field can't be empty");
					}
				}
				else {
					displayMessage("Name of the product can't be empty");
				}
			}
			
		});
		
		 compositeProductsToEdit.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				
				CompositeProduct prodSelected = (CompositeProduct)compositeProductsToEdit.getSelectedItem();
				ArrayList<MenuItem> prod = prodSelected.getComponents();
				baseProdInCompositeProdSelected.removeAllItems();
				
				for(MenuItem b : prod) {
					baseProdInCompositeProdSelected.addItem(b);
				}
			}
			
		});
		 
		 deleteSelected.addActionListener(new ActionListener() {
			 
			public void actionPerformed(ActionEvent arg0) {
				MenuItem prodToDelete = (MenuItem)baseProdInCompositeProdSelected.getSelectedItem();
				CompositeProduct toEdit = (CompositeProduct)compositeProductsToEdit.getSelectedItem();
				restaurantField.editCompositeProduct(toEdit, prodToDelete);
				toEdit.setPrice(toEdit.computePrice());
				displayMessage(prodToDelete.getName() + " deleted from the composition of " + compositeProductsToEdit.getSelectedItem());
				baseProdInCompositeProdSelected.removeItem(prodToDelete);
			}
			 
		 });
		 
		 editComposite.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				String newName =  newNameCompositeTF.getText();
				if(!newName.isEmpty()) {
					restaurantField.editProductName((CompositeProduct)compositeProductsToEdit.getSelectedItem(), newName);
				}
				displayMessage("Item selected was edited!");
			}
			 
		 });
		 
		 editBase.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				String newName = newNameBaseTF.getText();
				String newPriceString = newPriceTF.getText();
				MenuItem itemToEdit = (MenuItem)baseProductsToEdit.getSelectedItem();
				if(!newName.isEmpty()) {
					restaurantField.editProductName(itemToEdit, newName);
					displayMessage("Name edited");
				}
				if(!newPriceString.isEmpty()) {
					float newPrice;
					try {
						newPrice = Float.valueOf(newPriceString);
						restaurantField.editProductPrice(itemToEdit, newPrice);
						displayMessage("Price edited");
					}catch(NumberFormatException e) {
						displayMessage("Invalid price:" + e.getMessage());
					}
				}
				newNameBaseTF.setText(null);
				newPriceTF.setText(null);
			}
			 
		 });
	}
	
	public void backListener(ActionListener listener) {
		back.addActionListener(listener);
	}	

}
