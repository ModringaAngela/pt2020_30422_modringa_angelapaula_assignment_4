package view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public abstract class GUI extends JFrame{
	
	
	
	public GUI(int length, int width, String title) {
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(length, width);
		this.setTitle(title);
	}
	
	public abstract void setBoundsForElements();
	
	public abstract void addToPanel(JPanel panel);
	
	public void displayMessage(String message) {
		JOptionPane.showMessageDialog(this, message);
	}
}
