package view;
import java.awt.event.*;

import javax.swing.*;

import model.Restaurant;

public class StartGUI extends GUI{
	
	public JButton administrator = new JButton("Administrator");
	public JButton waiter = new JButton("Waiter");
	public JButton saveInformations = new JButton("Save");
	public JButton viewRestaurantInformations = new JButton("View Informations");
	
	private Restaurant restaurant;
	
	public StartGUI(Restaurant restaurant){
		
		super(400, 200, "LOG IN");
		
		this.restaurant = restaurant;
		
		JPanel start = new JPanel();
		start.setLayout(null);
		
		this.setBoundsForElements();
		this.addToPanel(start);
		this.add(start);
	}
	

	@Override
	public void setBoundsForElements() {
		administrator.setBounds(20, 100, 150, 30);
		waiter.setBounds(230, 100, 150, 30);
		saveInformations.setBounds(300, 50, 80, 30);
		viewRestaurantInformations.setBounds(20, 50, 150, 30);
	}

	@Override
	public void addToPanel(JPanel start) {
		start.add(administrator);
		start.add(waiter);
		start.add(saveInformations);
		start.add(viewRestaurantInformations);
	}
	
	public void administratorButtonListener(ActionListener listener) {
		administrator.addActionListener(listener);
	}
	
	public void waiterButtonListener(ActionListener listener) {
		waiter.addActionListener(listener);
	}
	
	public void saveActionListener(ActionListener l) {
		saveInformations.addActionListener(l);
	}
	
	public void viewActionListener(ActionListener l) {
		viewRestaurantInformations.addActionListener(l);
	}
	
}
