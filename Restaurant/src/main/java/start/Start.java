package start;

import model.Date;
import model.InvalidOperationException;
import model.MenuItem;
import model.BadDateException;
import model.BaseProduct;
import model.CompositeProduct;

import java.util.*;
import model.Order;
import model.Restaurant;
import view.*;
import controller.*;


public class Start {
	
	public static void main(String[] args) {
		
		
		String fileName = args[0];
		/*
		Restaurant restaurant = new Restaurant();
		Serializator.serialize(restaurant);
		restaurant = Serializator.deserialize(fileName);
		*/
		
		
		Restaurant restaurant = Serializator.deserialize(fileName);
		ChefGUI chef = new ChefGUI();
		restaurant.addObserver(chef);
		Controller controller = new Controller(restaurant);
		
	}
	
}
